import re
import os

# Global data_list list to store the data from data.txt
data_list = []
"""Global data_list list to store the data from data.txt."""

# Load data into data_list list
def LoadDataIntoList():
    """Load data into the global data_list list/array."""

    with open("data.txt") as file:
        if os.stat("data.txt").st_size != 0:
            for item in file.readlines():
                data_list.append(item)
        else:
            return print("File \"data.txt\" is empty...")
        
        file.close()
    
    return data_list

# Take what's inside data_list and dump it into the data.txt file
def InsertDataIntoFile():
    """Take what's inside data_list and dump it into the data.txt file"""
    
    with open("data.txt", "w") as file:
        for item in data_list:
            file.write(item)
            
        file.close()

# Recount/reorder the list from 1, 2, 3, etc...
def Count():
    """Reorder the list from 1, 2, 3, etc..."""
    
    if len(data_list) != 0:
        for i in range(len(data_list)):
            
            # Since there shouldn't be an i that is 0
            i += 1
            
            # m = match
            m = re.match("^(\d+\.\s+)(.*)", data_list[int(i) - 1])
            
            # s = search, and replace
            s = data_list[int(i) - 1].replace(m.groups()[0], str(i) + ". ")
            data_list[int(i) - 1] = s
            
            # Update data.txt file
            InsertDataIntoFile()
    else:
        return print("There is no data...")

# Add data to data_list and then update data.txt file
def AddData():
    """The add data function."""
    
    last_line_number = []
    
    with open("data.txt", "r") as file:
        if os.stat("data.txt").st_size != 0:
            last_line_number = re.search("^(\d+)", file.readlines()[-1])
        else:
            last_line_number.append(0) # If data.txt file is empty append 0 to last_line_number and then increase it in append
            
        file.close()

    inp = input("Data to add: ")
    
    if len(inp):
    
        # Append the last_line_number + 1 to make the added item go to one step above the last number of the last item
        data_list.append(str((int(last_line_number[0]) + 1)) + ". " + inp + "\n")
        
        # Update data.txt file
        InsertDataIntoFile()
        
        #Let the user know that the item has been added to the data.txt file
        print(str((int(last_line_number[0]) + 1)) + ". " + inp + ", has been added.")
        
    else:
        print("Input cannot be empty...")

# Display data in console
def DisplayData():
    """The display data function."""
    
    if len(data_list) != 0:
        for item in data_list:
            print(item.replace("\n", ""))
    else:
        return print("There is no data...")

# Delete an item in data_list and update data.txt
def DeleteDataItem():
    """The delete data function."""
    
    inp = input("Type the number of the entry to delete: ")
    
    # Check whether input is empty
    if len(inp):
        
        # Check whether input was a number
        if re.search("[1-9]", inp):
        
            # If input is not greater than the number of the last entry in data_list
            if not int(inp) > (len(data_list)):
                
                # If input is not 0
                if inp != "0":
                    
                    # If the data.txt is not empty
                    if os.stat("data.txt").st_size != 0:
                        item = data_list[int(inp) - 1]
                        data_list.remove(item)
                        
                        # Update data.txt file
                        InsertDataIntoFile()
                        
                        #Print and remove new-lines (\n), and let the user know which item was deleted
                        print(item.replace('\n', '') + ", has been removed.")
                        
                        # Reorder the data_list and data.txt to go from 1, 2, 3, etc...
                        Count()
                    else:
                        return print("File \"data.txt\" is empty...")
                else:
                    return print("Cannot choose 0 as an entry to delete...")
            else:
                return print("Number cannot be above " + str(len(data_list)) + "...")
        else:
            return print("Input wasn't a number...")
    else:
        return print("Input cannot be empty...")

# Swap two data items
def Swap():
    """Swap between two entries in data.txt."""
    
    # If the data_list list is not empty
    if len(data_list) != 0:
        inp_swap_1 = input("Enter the number of the entry to swap with: ")
        inp_swap_2 = input("Enter the second number of the entry to swap with: ")
        
        # Check if the inputs are numbers
        if inp_swap_1.isdigit() and inp_swap_2.isdigit():
        
            # First check if the input was the number 0
            if(inp_swap_1 != "0" or inp_swap_2 != "0"):
                
                # If the length of the inputs are not 0
                if len(inp_swap_1) != 0 and len(inp_swap_2) != 0:
                    
                    # If the inputs are not the same number to swap with
                    if inp_swap_1 != inp_swap_2:
                        
                        # Checks if an input number is too great (above the data_list's length)
                        if not int(inp_swap_1) > (len(data_list)) and not int(inp_swap_2) > (len(data_list)):
                            
                            to_swap_with_1 = data_list[int(inp_swap_1) - 1]
                            to_swap_with_2 = data_list[int(inp_swap_2) - 1]
                            data_list[int(inp_swap_1) - 1] = to_swap_with_2
                            data_list[int(inp_swap_2) - 1] = to_swap_with_1
                            
                            # Update data.txt file
                            InsertDataIntoFile()
                            
                            # Reorder the data_list and data.txt to go from 1, 2, 3, etc...
                            Count()
                        else:
                            return print("Number cannot be above " + str(len(data_list)) + "...")
                    else:
                        return print("You cannot swap with the same number...")
                else:
                    return print("You didn't enter a number for an entry to swap with...")
            else:
                return print("You cannot swap with number 0, since there can't be a number 0...")
        else:
            return print("Input needs to be a number...")
    else:
        return print("There is no data...")
    
# Edit an entry
def Edit():
    """Edit an entry in data.txt."""
    
    # If the data_list list is not empty
    if len(data_list) != 0:
        inp = input("Enter the number of the entry to edit: ")
        
        # If input isn't empty
        if len(inp):
            
            #Check whether input is a digit      
            if inp.isdigit():
                
                inp = int(inp)
                
                # If input is not greater than the number of the last entry in data_list
                if not int(inp) > (len(data_list)):
                    
                    # If input is not 0
                    if inp != "0":
                        to_edit = re.search("(\d+\.\s+)(.*)", data_list[inp - 1])
                        inp_2 = input("Enter what to edit entry \"" + str(inp) + "\" with: ")
                        data_list[inp - 1] = to_edit.groups()[0] + inp_2 + "\n"
                        InsertDataIntoFile()
                    else:
                        return print("You cannot edit number 0, since there can't be a number 0...")
                else:
                    return print("Number cannot be above " + str(len(data_list)) + "...")
            else:
                return print("Input wasn't a number...")
        else:
            return print("Input cannot be empty...")
    else:
        return print("There is no data...")

# Variable to loop the menu
exit_menu_loop = False

# Load data into data_list list
LoadDataIntoList()

# Menu loop
while not exit_menu_loop:
    print("--------------------------------")
    print("--------------Menu--------------")
    print("1. Display Data")
    print("2. Add Data")
    print("3. Delete Data Item")
    print("4. Swap")
    print("5. Edit")
    print("0. Exit Program")
    print("--------------------------------")
    print("--------------------------------")
    
    inp = input(">> ")
    
    if(re.search("0", inp)):
        exit_menu_loop = True
    elif(re.search("1", inp)):
        DisplayData()
    elif(re.search("2", inp)):
        AddData()
    elif(re.search("3", inp)):
        DeleteDataItem()
    elif(re.search("4", inp)):
        Swap()
    elif(re.search("5", inp)):
        Edit()
    else:
        print(f"Menu option { inp } doesn't exist...")
        